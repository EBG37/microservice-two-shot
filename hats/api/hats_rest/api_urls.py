from django.urls import path
from .views import api_hats  # api_delete_hat


urlpatterns = [
    path("hats/", api_hats, name="api_hats"),
    # path("hats/<int:pk>/", api_delete_hat, name="api_delete_hat"),
]
