from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
# from django.shortcuts import render
import json
from common.json import ModelEncoder
from .models import LocationVO, Hat

# Create your views here.


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href",
    ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "fabric",
        "color",
        "pictureURL",
        "location",
        "id",
    ]
    encoders = {"location": LocationVODetailEncoder()}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "fabric",
        "color",
        "pictureURL",
        "location",
        "id",
    ]
    encoders = {"location": LocationVODetailEncoder()}


@require_http_methods(["GET", "POST"])
def api_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
           {"hats": hats},
           encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        # print(content)
        try:
            location_href = content["location"]
            # print(location_href)
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location ID"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    #     pass


@require_http_methods("DELETE")
def api_delete_hat(request):
    if request.method == "DELETE":
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted", count > 0})
