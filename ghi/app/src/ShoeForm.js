import React, {useEffect, useState} from "react"

function ShoeForm() {

  const handleSubmit = async (event) => {
    event.preventDefault()

    const data = {}
    data.manufacturer = manufacturer
    data.model_name = model
    data.color = color
    data.picture_url = picture
    data.bin = bin


    const shoeUrl = "http://localhost:8080/api/shoes/"
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    }

    const response = await fetch(shoeUrl, fetchConfig)
    if (response.ok) {
      const newBin = await response.json()

      setManufacturer('')
      setModel('')
      setColor('')
      setPicture('')
      setBin('')
    }

  }

  const [bins, setBins] = useState([])

  const fetchData = async () => {
    const url = "http://localhost:8100/api/bins"

    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      setBins(data.bins)
    }
  }

  const [manufacturer, setManufacturer] = useState('')
  const [model, setModel] = useState('')
  const [color, setColor] = useState('')
  const [picture, setPicture] = useState('')
  const [bin, setBin] = useState('')

  const handleManufacturerChange = (event) => {
    const value = event.target.value
    setManufacturer(value)
  }
  const handleModelChange = (event) => {
    const value = event.target.value
    setModel(value)
  }
  const handleColorChange = (event) => {
    const value = event.target.value
    setColor(value)
  }
  const handlePictureChange = (event) => {
    const value = event.target.value
    setPicture(value)
  }
  const handleBinChange = (event) => {
    const value = event.target.value
    setBin(value)
  }

  useEffect(() => {
    fetchData()
    }, [])

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new shoe</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" value={manufacturer} id="manufacturer" className="form-control" />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleModelChange} placeholder="Model" required type="text" name="model_name" value={model} id="model_name" className="form-control" />
                <label htmlFor="model_name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} placeholder="color" required type="text" name="color" value={color} id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureChange} placeholder="Picture URL" required type="url" name="picture_url" value={picture} id="presenter_name" className="form-control" />
                <label htmlFor="picture_url">Picture</label>
              </div>
              <div className="mb-3">
                <select onChange={handleBinChange} required id="bin" name="bin" value={bin} className="form-select">
                  <option value="">Choose Bin</option>
                    {bins.map(bin => {
                      return (
                          <option key={bin.id} value={bin.href}>
                              {bin.closet_name}
                          </option>
                        )
                  })}
                </select>
              </div>
              <button className="btn btn-warning">Add</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}
export default ShoeForm