import { useState, useEffect } from "react"

function ShoesList() {
    const [shoes, setShoes] = useState([])

    const fetchData = async () => {
        const response = await fetch('http://localhost:8080/api/shoes')

        if (response.ok) {
            const data = await response.json()
            setShoes(data.shoes)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const deleteShoe = async (id) => {
        const url = `http://localhost:8080/api/shoes/${id}/`
        const fetchConfig = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            fetchData()
        }
    }

    return (
        <table className="table table-stripped table-hover align-middle">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Bin</th>
                    <th>Delete?</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe => {
                    return (
                        <tr key={shoe.id}>
                            <td>{ shoe.manufacturer }</td>
                            <td>{ shoe.model_name }</td>
                            <td>{ shoe.color }</td>
                            <td><img src={ shoe.picture_url } className="img w-25" /></td>
                            <td>{ shoe.bin.bin_number }</td>
                            <td>
                                <button type="button" className="btn btn-danger button-hover" id={shoe.id} onClick={() => deleteShoe(shoe.id)}>
                                    Delete Shoes?
                                </button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}

export default ShoesList