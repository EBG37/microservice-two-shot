import { Link } from 'react-router-dom';
function HatsList (props) {
    // const
    
    
    return (
        <>
            <table>
                <thead>
                    <tr>
                        <th>Picture</th>
                        <th>Name</th>
                        <th>Location</th>
                        <th>Fabric</th>
                        <th>Color</th>
                        <th>ID</th>
                    </tr>
                </thead>
                <tbody>
                    {/* { console.log("Value of props.hats in HatsList:", props.hats) } */}
                    {props.hats.map(hat => {
                        return (
                            <tr key={hat.id}> 
                                <td>
                                    <img src={ hat.pictureURL } style={{ width: '100px', height: 'auto' }} alt="single hat" />
                                </td>
                                <td>{ hat.style_name }</td>
                                <td>{ hat.location.closet_name }</td>
                                <td>{ hat.fabric }</td>
                                <td>{ hat.color }</td>
                                <td>{ hat.id }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div>
                <Link to="/hats/new" className="btn btn-primary">Add Hat</Link>
            </div>
        </>
            )
}

export default HatsList
