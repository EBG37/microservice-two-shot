import React, { useEffect } from "react";

function CreateHatForm() {
    
    return (
        <div className="row">
            <div className="shadow">
                <h1>Create Hat</h1>
                <form id="create-hat-form">
                    <div>
                        <label htmlFor ="style_name">Style</label>
                        <input placeholder="Style Name" required type ="text" name="style_name" id="style_name" className="form-control" />
                    </div>
                    <div>
                        <label htmlFor ="fabric">Fabric</label>
                        <input placeholder="Fabric" required type ="text" name="fabric" id="fabric" className="form-control" />
                    </div>
                    <div>
                        <label htmlFor ="hat_color">Color</label>
                        <input placeholder="hat_color" required type ="text" name="hat_color" id="hat_color" className="form-control" />
                    </div>
                    <div>
                        <label htmlFor ="pictureURL">Pic URL</label>
                        <input placeholder="pictureURL" required type ="text" name="pictureURL" id="pictureURL" className="form-control" />
                    </div>
                    <div>
                        <select required name="location" id="location" className="form-select">
                            <option selected value="">Choose a location</option>
                        </select>
                        {/* <input placeholder="Style Name" required type ="text" name="style_name" id="style_name" class="form-control" /> */}
                    </div>
                </form>
                <p>this is a create hat form</p>
            </div>
        </div>
    );

}

export default CreateHatForm
