from django.urls import path
from .views import api_list_shoes, api_delete_shoes


urlpatterns = [
    path("shoes/", api_list_shoes, name="list_shoes"),
    path("shoes/<int:pk>/", api_delete_shoes, name="delete_shoes")
 ]
