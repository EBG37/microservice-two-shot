# Wardrobify

Team:

* Person 1 - Everest - Shoes
* Person 2 - Spencer - Hats

## Design

## Shoes microservice

I created a Shoe Model that holds a manufacturer, a model, a color for the shoe, a picture of the shoe, and what bin and closet the shoe is located within the wardrobify project. The shoe microservice and the project are linked via a BinVO model that acts as a local Bin model within the shoes microservice.

Viewing shoes and deleting shoes take place in my ShoeList.js file. This file is represented by the Shoe List in my navigation. It call on the views in views.py file within the shoes_rest repository to shoe and delete shoes from the database.

Within that same views file, there is also a view to be able to create a new shoe. The form correlating with the create view is shown as Create a Shoe in the navigation bar. By filling out this form, You are able to create new shoes and add them to the database.
## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
